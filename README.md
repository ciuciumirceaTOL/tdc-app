# TDC App

This contains a blank Rails API only application with some configurations on it. It was created as a starting point for the 'Targul de Cariere' workshop. 

To install on Cloud9 run the script file in the terminal: 
```$
./script.sh
```
__The above script is custom for the Cloud9 environment. If you wish to run it some place else make sure to adapt the commads to your machine.__


